//
//  MainViewViewController.h
//  MapTest
//
//  Created by Scott on 10/10/12.
//  Copyright (c) 2012 Scott. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMMapView.h"

@interface MainViewViewController : UIViewController

@property (nonatomic, retain) RMMapView *mapView;;

@end