//
//  MainViewAppDelegate.h
//  MapTest
//
//  Created by Scott on 10/10/12.
//  Copyright (c) 2012 Scott. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;;

@end
