//
//  MainViewViewController.m
//  MapTest
//
//  Created by Scott on 10/10/12.
//  Copyright (c) 2012 Scott. All rights reserved.
//

#import "MainViewViewController.h"

@implementation MainViewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)loadView
{
    [self setMapView:[[RMMapView alloc]initWithFrame:CGRectMake(0.0, 0.0, 500, 500)]];
    [_mapView setBackgroundColor:[UIColor blackColor]];
    self.view = _mapView;
}

@end
