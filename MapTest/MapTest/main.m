//
//  main.m
//  MapTest
//
//  Created by Scott on 10/10/12.
//  Copyright (c) 2012 Scott. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MainViewAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MainViewAppDelegate class]));
    }
}
